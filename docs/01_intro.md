# Introduction

## useful links

* [Katacoda tuto](https://www.katacoda.com/courses/servicemesh)
* [Red Hat: What is a service mesh ?](https://www.redhat.com/fr/topics/microservices/what-is-a-service-mesh)

## Course objectives

* Configure Envoy Proxy to forward traffic to external websites
* Configure Envoy Proxy to forward traffic to Docker Container
* Perform path-based routing for controlling traffic destination

* Advanced configuration:
	* Load balancing
	* Health checking

## Description

Envoy is configured using a YAML definition file to control the proxy's behaviour. In this step, we're building a configuration using the Static Configuration API. This means that all the settings are pre-defined within the configuration.

Envoy also supports Dynamic Configuration. This allows the settings to be discovered via an external source.
