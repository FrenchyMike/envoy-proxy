# Configuration

**Resources/Listeners:**

The first line of the Envoy configuration defines the API configuration being used. In this case, we're configuring the Static API, so the first line should be static_resources. Copy the snippet to the editor.

The beginning of the configuration defines the Listeners. A Listener is the networking configuration, such as IP address and ports, that Envoy listens to for requests. Envoy runs inside of a Docker Container, so it needs to listen on the IP address 0.0.0.0. In this case, Envoy will listen on port 10000.

```yml
static_resources: # Static API configuration
  listeners:
  - name: listener_0
    address:
      socket_address: { address: 0.0.0.0, port_value: 10000 }
```

* `static_resources`: Static API configuration
* `listeners`: A listener is the networking configuration, such as IP address and port, that Envoy listens to for request. Envoy run inside a docker container, so it needs to listen on **0.0.0.0** on port **10000**

**Filter chains and filter:**

With Envoy listening for incoming traffic, the next stage is to define how to process the requests. Each Listener has a set of filters, and different Listeners can have a different set of filters.

In this example, we'll proxy all traffic to Google.com (thanks Google!). The result: We should be able to request the Envoy endpoint and see the Google homepage appear, without the URL changing.

Filtering is defined using filter_chains. The aim of each filter is to find a match on the incoming request, to match it to the target destination.


```yaml
    filter_chains:
    - filters:
      - name: envoy.http_connection_manager
        config:
          stat_prefix: ingress_http#
          route_config:
            name: local_route
            virtual_hosts:
            - name: local_service
              domains: ["*"]
              routes:
              - match: { prefix: "/" }
                route: { host_rewrite: www.google.com, cluster: service_google }
          http_filters:
          - name: envoy.router
```

* `filter_chains`: With Envoy listening for incoming traffic, the next stage is to define how to process the requests. Each Listener has a set of filters, and different Listeners can have a different set of filters.
  * `stat_prefix`: The human-readable prefix to use when emitting statistics for the connection manager.
  * `route_config`: The configuration for the route. If the virtual host matches, then the route is checked. In this example, the route_config matches all incoming HTTP requests, no matterthe host domain requested.
  * `routes`: If the URL prefix is matched then a set of route rules defines what should happen next. In this case "/" means match the root of the request
  * `host_rewrite`: Change the inbound Host header for the HTTP request.
  * `cluster`: The name of the cluster which will handle the request. The implementation is defined below.
  * `http_filters`: The filter allows Envoy to adapt and modify the request as it is processed.

**Clusters:**

When a request matches a filter, the request is passed onto a cluster. The cluster shown below defines that the host is google.com running over HTTPS. If multiple hosts had been defined, then Envoy would perform a Round Robin strategy.

```yml
  clusters:
  - name: service_google
    connect_timeout: 0.25s
    type: LOGICAL_DNS
    dns_lookup_family: V4_ONLY
    lb_policy: ROUND_ROBIN
    hosts: [{ socket_address: { address: google.com, port_value: 443 }}]
    tls_context: { sni: www.google.com }
```

**Admin:**

Finally, an admin section is required. The admin section is explained in more detail in the following steps.

```yaml
admin:
  access_log_path: /tmp/admin_access.log
  address:
    socket_address: { address: 0.0.0.0, port_value: 9901 }
```


## Start Envoy with docker

**Run the service:**

```bash
docker run --name=proxy -d \
  -p 80:10000 \
  -v $(pwd)/envoy/envoy.yml:/etc/envoy/envoy.yml \
  envoyproxy/envoy-dev:v1.20.0
```


**Run the admin interface:**

The admin interface will be run without any security compliance for a demonstration purpose: see [documentation here](https://www.envoyproxy.io/docs/envoy/latest/operations/admin) for enhanced security

```bash
docker run --name=proxy-with-admin -d \
    -p 9901:9901 \
    -p 10000:10000 \
    -v $(pwd)/envoy/envoy.yaml:/etc/envoy/envoy.yaml \
    envoyproxy/envoy:latest
```

* [Envoy configuration best practices](https://www.envoyproxy.io/docs/envoy/latest/configuration/best_practices/best_practices)