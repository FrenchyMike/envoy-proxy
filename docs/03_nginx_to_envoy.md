# Migrate from nginx to to envoy

__NGINX__ configurations generally have three key elements:
1. Configuring the NGINX server, logging structure and Gzip functionality. This is defined globally across all instances.
2. Configuring NGINX to accept requests for one.example.com host on port 8080.
3. Configuring the target location of how to handle traffic to different parts of the URL.

__Envoy Proxy__ has four key types that support the core infrastructure offered by NGINX. The core is:
* `Listeners`: They define how Envoy Proxy accepts incoming requests. At present, Envoy Proxy only supports TCP-based listeners. Once a connection is made, it’s passed on to a set offilters for processing.
* `Filters`: They are part of a pipeline architecture that can process inbound and outbound data. This functionality enables filters such as Gzip which compresses data before sending it tothe client.
* `Routers`: These forward traffic to the required destination, defined as a cluster.
* `Clusters`: They define the target endpoint for traffic and the configuration settings.

## Example

* The [nginx.conf](../appendices/nginx.conf) that will be used through this section

### Worker connections
* Nginx: directly declared in the config file
  ```nginx
  worker_processes  2;

    events {
    worker_connections   2000;
    }
  ```
* Envoy: Spawns a worker threads for every hardware thread in the system. Each worker thread runs a non-blocking event loop that is responsible for
  1. Listening on every listener
  2. Accepting new connections
  3. Instantiating a filter stack for the connection
  4. Processing all IO for the lifetime of the connection.

All connection pools in Envoy are per worker thread. Though HTTP/2 connection pools only make a single connection to each upstream host at a time, if there are four workers, there will be four HTTP/2 connections per upstream host at steady state.<br/>
By keeping everything within a single worker thread, almost all the code can be written without locks and as if it is single threaded. Having more workers than necessary will waste memory, create more idle connections, and lead to a lower connection pool hit rate.

### HTTP Configuration

The next block of NGINX configuration defines the HTTP settings, such as:
* Which mime types are supported
* Default timeouts
* Gzip configuration

Within the HTTP configuration block, the NGINX configuration specifies to listen on port 8080 and respond to incoming requests for the domains one.example.com and www.one.example.com.

```nginx
server {
   listen        8080;
   server_name   one.example.com  www.one.example.com;
```

__Envoy listeners:__

The snippet below will create a new listener and bind it to port 8080.

The configuration indicates to Envoy Proxy which ports it should be bound to for incoming requests.

```yaml
static_resources:
  listeners:
  - name: listener_0
    address:
      socket_address: { address: 0.0.0.0, port_value: 8080 }
```
There is no need to define the server_name as Envoy Proxy filters will handle this.

### Local configuration

When a request comes into NGINX, a location block defines how to process and where to forward the traffic. In the following snippet, all the traffic to the site is proxied to an upstream cluster called targetCluster.<br/>
The upstream cluster defines the nodes that should process the request. We will discuss this in the next step.

```nginx
location / {
    proxy_pass         http://targetCluster/;
    proxy_redirect     off;

    proxy_set_header   Host             $host;
    proxy_set_header   X-Real-IP        $remote_addr;
}
```
Within Envoy, this is managed by Filters.

__Envoy Filters:__

For the static configuration, the filters define how to handle incoming requests. In this case, we are setting the filters that match the server_names in the previous step.<br/>
When incoming requests are received that match the defined domains and routes, the traffic is forwarded to the cluster. This is the equivalent of the upstream NGINX configuration.

```yaml
    filter_chains:
    - filters:
      - name: envoy.filters.network.http_connection_manager
        typed_config:
          "@type": type.googleapis.com/envoy.extensions.filters.network.http_connection_manager.v3.HttpConnectionManager
          stat_prefix: ingress_http
          codec_type: AUTO
          route_config:
            name: local_route
            virtual_hosts:
            - name: local_service
              domains: ["*"]
              routes:
              - match: { prefix: "/" }
                route: { cluster: targetCluster }
          http_filters:
          - name: envoy.filters.http.router
```
The name `envoy.http_connection_manager` is a built-in filter within Envoy Proxy.<br/>
Other filters include Redis, Mongo, TCP. You can find the complete list in the [documentation](https://www.envoyproxy.io/docs/envoy/latest/configuration/listeners/listener_filters/listener_filters).<br/>
For more information about other load balancing policies visit the [Envoy documentation](https://www.envoyproxy.io/docs/envoy/v1.8.0/intro/arch_overview/load_balancing).

### Proxy and upstream configuration

Within NGINX, the upstream configuration defines the set of target servers that will handle the traffic. In this case, two clusters have been assigned.
```nginx
  upstream targetCluster {
    172.18.0.3:80;
    172.18.0.4:80;
  }
```

__Envoy Cluster:__
The equivalent of upstream is defined as Clusters. In this case, the hosts that will serve the traffic have been defined.<br/>
The way the hosts are accessed, such as the timeouts, are defined as the cluster configuration.<br/>
This allows finer grain control over aspects such as timeouts and load balancing.
```yaml
  clusters:
  - name: targetCluster
    connect_timeout: 0.25s
    type: STRICT_DNS
    lb_policy: ROUND_ROBIN
    load_assignment:
      cluster_name: targetCluster
      endpoints:
      - lb_endpoints:
        - endpoint:
            address:
              socket_address:
                address: 172.17.0.3
                port_value: 80
        - endpoint:
            address:
              socket_address:
                address: 172.17.0.4
                port_value: 80
```
When using `STRICT_DNS` service discovery, Envoy will continuously and asynchronously resolve the specified DNS targets.<br/>
Each returned IP address in the DNS result will be considered an explicit host in the upstream cluster.<br/>
This means that if the query returns two IP addresses, Envoy will assume the cluster has two hosts, and both should be load balanced to.<br/>
If a host is removed from the result, Envoy assumes it no longer exists and will drain traffic from any existing connection pools.

For further information, refer to [Envoy Proxy documentation](https://www.envoyproxy.io/docs/envoy/v1.8.0/intro/arch_overview/service_discovery#arch-overview-service-discovery-types-strict-dns).

### Logging Access and Errors

The final configuration is the logging. Instead of piping the error logs to disk, Envoy Proxy follows a cloud-native approach. 
<br/>All the application logs are outputted to stdout and stderr.
<br/>.When users make a request, the access logs are optional and disabled by default. 
<br/>To enable access logs for HTTP requests, include an access_log configuration for the HTTP Connection Manager. 
<br/>The path can be either a device, such as stdout, or a file on disk, depending on your requirements.

The following configuration will pipe all access logs to stdout:
```yaml
access_log:
- name: envoy.file_access_log
  config:
    path: "/dev/stdout"
```

By default, Envoy has a format string that includes details of the HTTP request:

```log
[%START_TIME%] "%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%"
%RESPONSE_CODE% %RESPONSE_FLAGS% %BYTES_RECEIVED% %BYTES_SENT% %DURATION%
%RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% "%REQ(X-FORWARDED-FOR)%" "%REQ(USER-AGENT)%"
"%REQ(X-REQUEST-ID)%" "%REQ(:AUTHORITY)%" "%UPSTREAM_HOST%"\n
```

The result of this format string is:

```log
[2018-11-23T04:51:00.281Z] "GET / HTTP/1.1" 200 - 0 58 4 1 "-" "curl/7.47.0" "f21ebd42-6770-4aa5-88d4-e56118165a7d" "one.example.com" "172.18.0.4:80"
```

The contents of the output can be customised by setting the format field. For example:

```yaml
access_log:
- name: envoy.file_access_log
  config:
    path: "/dev/stdout"
    format: "[%START_TIME%] "%REQ(:METHOD)% %REQ(X-ENVOY-ORIGINAL-PATH?:PATH)% %PROTOCOL%" %RESPONSE_CODE% %RESP(X-ENVOY-UPSTREAM-SERVICE-TIME)% "%REQ(X-REQUEST-ID)%" "%REQ(:AUTHORITY)%" "%UPSTREAM_HOST%"\n"
```

The log line can also be outputted as JSON by setting the json_format field. For example:

```yaml
access_log:
- name: envoy.file_access_log
  config:
    path: "/dev/stdout"
    json_format: {"protocol": "%PROTOCOL%", "duration": "%DURATION%", "request_method": "%REQ(:METHOD)%"}
```

For further information on Envoy's logging approach, visit : https://www.envoyproxy.io/docs/envoy/v1.8.0/configuration/access_log#config-access-log

<br/>Logging isn't the only way to gain visibility into production with Envoy Proxy.
<br/>Advanced tracing and metrics capabilities are built into it.
<br/>You can find out more in the tracing documentation or via the [Interactive Tracing Scenario](https://www.envoyproxy.io/try/implementing-metrics-tracing).